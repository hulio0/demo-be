FROM julioupm/alpine-openjdk11-maven

WORKDIR /app
COPY pom.xml .
COPY src ./src

RUN mvn package -Dmaven.test.skip=true

CMD ["mvn","spring-boot:run"]

