package app.dao.counter;

import app.dao.connection.DatabaseConnector;
import app.model.counter.Counter;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;

@Repository
public class CounterRepository {

    private static final QueryRunner RUNNER = new QueryRunner();

    @Autowired
    private DatabaseConnector connector;

    public Counter getCounter() {
       BeanHandler<Counter> handler = new BeanHandler<>(Counter.class);
       String sql = "SELECT * FROM counter";

       Counter res = null;
       try( Connection conn = this.connector.getConnection() )
       {
          res = this.RUNNER.query(conn,sql,handler);
       } catch (Exception e) { e.printStackTrace(); }
       return res;
    }

    public void setCounter(Counter newCounter) {
       String sql = "UPDATE counter SET value=?";
       try( Connection conn = this.connector.getConnection() )
       {
           this.RUNNER.update(conn,sql,newCounter.getValue());
       } catch(Exception ex) { ex.printStackTrace(); }
    }
}
