package app.dao.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.springframework.stereotype.Component;

@Component
public class DatabaseConnector {
  public static final String DB_URL =
    "jdbc:mysql://ordenador.westeurope.cloudapp.azure.com:5000/demo?user=root&password=admin";

  public Connection getConnection() {
    Connection conn = null;

    try {
      conn = DriverManager.getConnection(DB_URL);
    } catch (SQLException ex) {
      ex.printStackTrace();
    }

    return conn;
  }
}
