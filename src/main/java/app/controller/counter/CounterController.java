package app.controller.counter;

import app.model.counter.Counter;
import app.service.counter.CounterService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CounterController {
  @Autowired
  private CounterService counterService;

  @GetMapping("/counter")
  public Counter getCounter() {
    return this.counterService.getCounter();
  }

  @PostMapping("/counter")
  public void setCounter(@RequestBody Counter resp) {
    this.counterService.setCounter(resp);
  }

  @PutMapping("/counter")
  public void incrementCounter(@RequestBody String body) {
    this.counterService.incrementCounter(
        new JSONObject(body).getInt("increment")
    );
  }
}
