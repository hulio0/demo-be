package app.service.counter;

import app.dao.counter.CounterRepository;
import app.model.counter.Counter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CounterService {
  @Autowired
  private CounterRepository counterRepository;

  public Counter getCounter() {
    return this.counterRepository.getCounter();
  }

  public void setCounter(Counter newCounter) {
    this.counterRepository.setCounter(newCounter);
  }

  public void incrementCounter(Integer increment) {
    Integer currentValue = this.getCounter().getValue();
    Counter newCounter = new Counter(currentValue + increment);
    this.setCounter(newCounter);
  }
}
